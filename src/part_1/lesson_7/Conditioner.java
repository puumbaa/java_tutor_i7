package part_1.lesson_7;

public class Conditioner implements IkPortCompatible{
    @Override
    public String turnOn() {
        return "Conditioner turned on";
    }

    @Override
    public String turnOff() {
        return "Conditioner turned off";
    }
}
