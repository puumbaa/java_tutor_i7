package part_1.lesson_7;

public interface IkPortCompatible {

    String turnOn();
    String turnOff();
}
