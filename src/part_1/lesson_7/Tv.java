package part_1.lesson_7;

public class Tv implements IkPortCompatible{



    @Override
    public String turnOn() {
        return "Tv turned on";
    }

    @Override
    public String turnOff() {
        return "Tv turned off";
    }
}
