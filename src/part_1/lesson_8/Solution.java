package part_1.lesson_8;

public class Solution {
    int t;
    static String a;
    static int cnt = 0;
    static final String temp = "ASD";

    public Solution() {
        cnt++;
    }


    public int getCntOfInstances() {
        return cnt;
    }

    public static void main(String[] args) {
        Solution solution = new Solution();
//        t = 5;
//        say();
//        parseImage("");

        solution.say();
        solution.say("Andrew");

        Parent parent = new Parent();

        Parent child = new Child(); // восходящее преобразование
//        child.blabla();

        Child child1 = (Child) child; // нисходящее преобразование
        child1.blabla();

        solution.bar(new Parent[]{parent, child});

    }

    public void bar(Parent[] parents) {
        for (Parent p : parents) {
            if (p.getClass() == Child.class) {
                //do something
            }
            if (p instanceof SecondChild){
                //do something
            }
            p.saySomething(); //позднее связывание
        }
    }

    // раннее связывание
    public void say() {
        System.out.println("hello");
    }

    public void say(String name) {
        System.out.println("hello my name is " + name);
    }
}
