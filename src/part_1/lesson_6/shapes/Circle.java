package part_1.lesson_6.shapes;

public class Circle extends Shape{
    private double radius;
    private final double PI = Math.PI;


    public Circle(double radius) {
        this.radius = radius;
    }

    @Override
    public double getSquare() {
        return 2*PI*radius*radius;
    }

}
