package part_1.lesson_6.animals;

public class Cat extends Feline {
    @Override
    void makeNoise() {
        System.out.println("meow");
    }

    @Override
    void eat() {
        System.out.println("кушаю по кошачьи");
    }
}
