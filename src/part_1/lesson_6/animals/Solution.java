package part_1.lesson_6.animals;

import part_1.lesson_6.shapes.Circle;
import part_1.lesson_6.shapes.Rectangle;
import part_1.lesson_6.shapes.Shape;

public class Solution {
    public static void main(String[] args) {
/*        Animal cat = new Cat();
        Animal dog = new Dog();
        Animal hippo = new Hippo();
        Animal[] animals = {cat,dog,hippo};

        for (Animal animal : animals) {
            animal.roam();
        }*/
        Shape s1 = new Circle(5);
        Shape s2 = new Rectangle(5,5);
        Shape[] shapes = new Shape[2];

        shapes[0] = s1;
        shapes[1] = s2;
        for (Shape shape: shapes){
            System.out.println(shape.getSquare());
        }
    }
}
