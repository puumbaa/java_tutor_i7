package part_1.lesson_6.animals;

public abstract class Animal {
    protected String food; // что употребляет в пищу [мясо,трава]
    protected int hunger; // уровень голода [0,10]
    protected String location; // где обитает

    abstract void makeNoise(); // поведение животного, когда оно должно издавать звук

    void sleep(){
        // поведение животного, когда оно решает поспать
        System.out.println("sleep...");
    }
    abstract void eat(); // поведение животного при обнаружение своей пищи (мясо или трава)
    abstract void roam(); // поведение животного, когда оно не ест  и не спит

}
