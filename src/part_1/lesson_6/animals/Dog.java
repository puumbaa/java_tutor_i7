package part_1.lesson_6.animals;

public class Dog extends Canine{
    @Override
    void makeNoise() {
        System.out.println("bark bark!");
    }

    @Override
    void eat() {

    }
}
