package part_1.lesson_9.homeTheatre.src.components;

public class PopcornPopper {
    public void on() {
        System.out.println("Popcorn Popper on");
    }

    public void off() {
        System.out.println("Popcorn Popper off");
    }

    public void pop() {
        System.out.println("Popcorn Popper popping popcorn");
    }
}
