package part_1.lesson_9.homeTheatre.src;

import part_1.lesson_9.homeTheatre.src.components.*;

public class HomeTheatreFacade {

    Player simplePlayerImpl;
    Projector projector;
    TheaterLights lights;
    Screen screen;
    PopcornPopper popper;

    public HomeTheatreFacade(Player simplePlayerImpl, Projector projector,
                             TheaterLights lights, Screen screen, PopcornPopper popper)
    {


        this.simplePlayerImpl = simplePlayerImpl;
        this.projector = projector;
        this.lights = lights;
        this.screen = screen;
        this.popper = popper;
    }

    public void watchMovie(Movie movie) {
        System.out.println("Get ready to watch a movie...");
        popper.on();
        popper.pop();
        lights.dim(10);
        screen.down();
        projector.on();
        projector.wideScreenMode();
        simplePlayerImpl.on();
        simplePlayerImpl.play(movie);
    }

    public void endMovie()
    {
        System.out.println("Shutting movie theater down...");
        popper.off();
        lights.on();
        screen.up();
        projector.off();
        simplePlayerImpl.stop();
        simplePlayerImpl.eject();
        simplePlayerImpl.off();
    }
}
