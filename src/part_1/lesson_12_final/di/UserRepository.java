package part_1.lesson_12_final.di;

public interface UserRepository {
    void save(User user);
}
