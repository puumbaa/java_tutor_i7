package part_1.lesson_12_final.task;

import java.util.Objects;

import static part_1.lesson_12_final.task.Point.getLength;

public class Triangle {


    private Point a;
    private Point b;
    private Point c;
    private double aSide;
    private double bSide;
    private double cSide;

    // ab == ac, ab == bc,
    public Triangle(Point a, Point b, Point c) {
        // checking
        if (getLength(a, c) == getLength(a, b) || getLength(a, c) == getLength(b, c) ||
                getLength(a, b) == getLength(b, c)) {
            this.a = a;
            this.b = b;
            this.c = c;
            aSide = getLength(b, c);
            bSide = getLength(this.a, c);
            cSide = getLength(this.a, this.b);
        }
        throw new IllegalArgumentException("wrong triangle!");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Triangle triangle = (Triangle) o;

        return Objects.equals(a, triangle.a) && Objects.equals(b, triangle.b) && Objects.equals(c, triangle.c)
                || additionalCondition(triangle);
    }

    private boolean additionalCondition(Triangle triangle) {
        double aSide = getLength(triangle.b, triangle.c);
        double bSide = getLength(triangle.a, triangle.c);
        double cSide = getLength(triangle.a, triangle.b);
        
        return aSide == this.aSide && bSide == this.bSide && cSide == this.cSide;
        // разобрать 2 случая)
            
        
    }


    double perimetr() {
        double aSide = getLength(b, c);
        double bSide = getLength(this.a, c);
        double cSide = getLength(this.a, this.b);
        return aSide + bSide + cSide;
    }

    double square() {
        double aSide = getLength(b, c);
        double bSide = getLength(this.a, c);
        double cSide = getLength(this.a, this.b);
        double p = perimetr() / 2;
        return Math.sqrt(p*(p-aSide)*(p-bSide)*(p-cSide));
    }

    double[] getAngles() {

        double aSide = getLength(b, c);
        double bSide = getLength(this.a, c);
        double cSide = getLength(this.a, this.b);
        double aAngleCos = (bSide*bSide + cSide*cSide - aSide*aSide) / 2 * bSide * cSide;
        double bAngleCos = (aSide*aSide + cSide*cSide - bSide*bSide) / 2 * aSide * bSide;
        double cAngleCos = (aSide*aSide + bSide*bSide - cSide*cSide) / 2 * aSide * bSide;
        return new double[]{Math.acos(aAngleCos), Math.acos(bAngleCos), Math.acos(cAngleCos)};
    }

    @Override
    public String toString() {
        return "";
    }
}

class TestDrive {
    public static void main(String[] args) {
    }

    static double getPerimetr(Triangle[] arr) {
        double result = 0.0;
        for(Triangle t : arr){
            result += t.perimetr();
        }
        return result;
    }

    boolean check(Triangle[] arr) { // хотя бы 2 равны
        int cnt = 0;
        for (int i = 0; i < arr.length-1; i++) {
            for (int j = i+1; j < arr.length; j++) {
                if (arr[i].equals(arr[j])) cnt++;
                if (cnt == 2) return true;
            }
        }
        return false;
    }
}
