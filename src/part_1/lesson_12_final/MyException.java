package part_1.lesson_12_final;

public class MyException  extends Exception {
    private String message;

    public MyException(String message) {
        super(message);
    }
}
