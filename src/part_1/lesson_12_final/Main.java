package part_1.lesson_12_final;

public class Main {
    public static void main(String[] args) {
        Shape a = new Squad(3);
        System.out.println(a.square());
//        System.out.println(squad.square()); // 12 = 3 * 4

    }
}

interface Shape {
    int square();
}

class Rectangle implements Shape {
    int a;
    int b;

    public int square(){return a*b;}

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }
}
class Squad implements Shape {
    public Squad(int a) {
        this.a = a;
    }

    int a;
    public void setA(int a) {
        this.a = a;
    }

    @Override
    public int square() {
        return a * a;
    }
}