package part_1.lesson_5;

import java.util.Objects;

public class Human {

    public static String someField = "bla-bla";
    private String firstName;
    private String lastName;
    private int age = 50;
    private int bar;

    public Human(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return "firstname: " + firstName + " \n lastname: "+ lastName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return age == human.age && Objects.equals(firstName, human.firstName) && Objects.equals(lastName, human.lastName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, age);
    }

    public static void someMethod(){
        System.out.println("foo");
        final int c = 10;
    }

    public void wakeUp(){
        innerProcess1();
        innerProcess2();
        System.out.println("I waked up!");
    }


    private void innerProcess1(){}
    private void innerProcess2(){}

    public Human(int age) {
        this.age = age;
    }

    public Human(String _firstName, String _lastName, int _age, int bar) {
        this(_age);
        firstName = _firstName;
        lastName = _lastName;
        this.bar = bar;
    }


    public void showInfo(){
        System.out.println("I'm human, my name is " + firstName);
    }

    public void showInfo(String phoneNumber){
        showInfo();
        System.out.println("my phone number: " + phoneNumber);
    }


    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


    public void setFirstName(String firstName){
        this.firstName = firstName;
    }



    public String getFirstName(){
        return firstName;
    }

    public void setAge(int _age){
        if (_age > 0) {
            age = _age;
        }
    }

    public int getAge(){
        return age;
    }

    public Human(){}


}
