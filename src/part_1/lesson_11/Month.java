package part_1.lesson_11;

public enum Month {
    JANUARY(1), FEBRUARY(2), MART(3),
    APRIL(4), MAY(5), JUNE(6), JULE(7),
    AUGUST(8);


    public int getNumber() {
        return number;
    }

    public void showNumber(){
        System.out.println("My number is " +  number);
    }

    private final int number;

    Month(int number) {
        this.number = number;
    }
}
