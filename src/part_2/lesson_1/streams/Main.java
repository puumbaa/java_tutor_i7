package part_2.lesson_1.streams;

import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {

//        DataConverter converter = num -> "Число " + num;
//        DataConverter converter = () -> System.out.println("метод без параметров");
//        testLyamba(converter);

        List<String> collect = Stream.of(1, 2, 3, 4, 5)
                .filter(integer -> integer > 2)
                .map(integer -> "Число: " + integer)
                .collect(Collectors.toList());



        System.out.println(collect);


    }
/*    public static void testLyamba(DataConverter converter){
        // if (event) {eventCallback.apply()}
        converter.convertSomething();
    }*/
}
