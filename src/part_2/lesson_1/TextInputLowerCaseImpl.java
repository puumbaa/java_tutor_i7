package part_2.lesson_1;

import java.util.Locale;

public class TextInputLowerCaseImpl extends TextInput {
    @Override
    public void onInput() {
        this.text = this.text.toLowerCase(Locale.ROOT);
    }
}
