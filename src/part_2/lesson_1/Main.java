package part_2.lesson_1;

import java.util.Locale;

public class Main {
    public static void main(String[] args) {
/*
        TextInput input = new TextInputLowerCaseImpl();
        input.input("Привет как дела? Что делаешь?");
        System.out.println(input.getText());*/

        TextInput input1 = new TextInput() {
            @Override
            public void onInput() {
                this.text = this.text.toUpperCase(Locale.ROOT);
            }
        };

        input1.input("Привет как дела? Что делаешь?");
        System.out.println(input1.text);
    }
}
