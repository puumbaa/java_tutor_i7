package part_2.lesson_1.order_task.service;


import part_2.lesson_1.order_task.entities.Item;
import part_2.lesson_1.order_task.entities.Order;
import part_2.lesson_1.order_task.entities.Status;
import part_2.lesson_1.order_task.entities.StatusGroup;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

//Вы проектируете систему для управления заказами интернет магазина. Вам необходимо реализовать интерфейс OrderService
public class OrderServiceImplementation implements OrderService {
    @Override
    public Order getOrderById(List<Order> orders, Long id) {
        return orders.stream()
                .filter(order -> order.getId().equals(id))
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }

    @Override
    public List<Order> getAllOrdersByStatus(List<Order> orders, Status status) {
        return orders.stream()
                .filter(order -> order.getStatus().equals(status))
                .collect(Collectors.toList());
    }

    @Override
    public List<Order> getAllOrdersByStatusGroup(List<Order> orders, StatusGroup statusGroup) {
        return orders.stream()
                .filter(order ->
                        (statusGroup.name().equals("IN_PROGRESS") && (order.getStatus().name().equals("NEW") ||
                                order.getStatus().name().equals("CONFIRMED"))) ||

                                (statusGroup.name().equals("FINAL") && (order.getStatus().name().equals("DELIVERED") ||
                                        order.getStatus().name().equals("CANCELLED"))))
                .collect(Collectors.toList());
    }

    @Override
    public List<Order> getAllOrdersWithoutDeletedItems(List<Order> orders) {
        return orders.stream()
                .filter(order -> order.getItems().stream().noneMatch(Item::getDeleted))
                .collect(Collectors.toList());
    }

    @Override
    public List<Order> getOrdersWithPriceGreaterThan(List<Order> orders, Double price) {
        return orders.stream()
                .filter(order -> order.getTotalPrice() > price)
                .collect(Collectors.toList());
    }

    @Override
    public Map<Long, Order> idAndOrderMap(Stream<Order> orderStream) {
        return orderStream
                .collect(Collectors.toMap(Order::getId, o2 -> o2));
    }

    @Override
    public Double calculateTheSum(List<Order> orders) {
        return orders.stream()
                .map(Order::getTotalPrice)
                .mapToDouble(value -> value)
                .sum();
    }

    // получаем список заказов - > мапу item и все заказы в которых он встречается
    @Override
    public Map<Item, List<Order>> itemAndListOfOrders(List<Order> orderList) {
/*      1 способ (не очень круто, так как используем мапу извне)
        Map<Item, List<Order>> result = new HashMap<>();
        orderList.forEach(order -> order.getItems()
                .forEach(item -> {
                    List<Order> orders = result.get(item);
                    if (orders == null) orders = new ArrayList<>();
                    orders.add(order);
                    result.put(item, orders);
                }));
        return result;*/

        // Берем стрим из order превращаем в стрим из item и собираем все item-ы в мапу,
        // где ключом будет текущий item, для получения значения:
        //    >  пробегаемся снова по orderList.stream()
        //    >  оставляем только те order у которых в списке item-ов содержится текущий item
        //    >  собираем все в лист

        return orderList.stream()
                .flatMap(order -> order.getItems().stream().distinct())
                .collect(Collectors.toMap(item -> item,
                        item -> orderList.stream()
                                .filter(order -> order.getItems().contains(item))
                                .collect(Collectors.toList())));
    }
}
