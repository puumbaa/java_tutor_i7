package part_2.lesson_1.order_task.entities;

import java.util.Arrays;
import java.util.List;

public enum StatusGroup {

	IN_PROGRESS(Arrays.asList(Status.NEW, Status.CONFIRMED), "В процессе"),
	FINAL(Arrays.asList(Status.DELIVERED, Status.CANCELLED), "Финальные");

	private List<Status> statuses;

	private String title;

	StatusGroup(List<Status> statuses, String title) {
		this.statuses = statuses;
		this.title = title;
	}
}
