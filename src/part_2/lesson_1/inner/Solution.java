package part_2.lesson_1.inner;

public class Solution {

    public static void main(String[] args) {
//         Table.TableEntry entry = new Table.TableEntry(1, "Эльнур");


        Table table = new Table();
        table.add(1,"Эльнур1");
        table.add(2,"Эльнур2");
        table.add(3,"Эльнур3");

        Table.TableIterator iterator = table.new TableIterator();
        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }
    }

}
