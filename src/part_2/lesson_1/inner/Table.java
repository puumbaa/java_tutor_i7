package part_2.lesson_1.inner;

/*
*
* -> | key | value|
* -> |   1  | Эльнур1|
* -> |   2  | Эльнур2| i = 1
* -> |   3  | Эльнур3| i = 2 | size = 3
*
* */

public class Table {

    public class TableIterator{
        int i = - 1; // чтобы сказать что начинаем перед 1ым элементом
        public TableEntry next(){
            i++;
            return entries[i];
        }
        public boolean hasNext(){
            return i < entries.length - 1;
        }
    }


    private TableEntry[] entries;
    private static final int MAX_SIZE = 10;
    private int count;

    public Table() {
        this.entries = new TableEntry[MAX_SIZE];
    }

    public void add(int key, String value) {
        if (entries != null && count < entries.length){
            entries[count] = new TableEntry(key,value);
            count++;
        }
    }


    private static class TableEntry {
        private int key;
        private String value;

        public TableEntry(int key, String value) {
            this.key = key;
            this.value = value;
        }

        @Override
        public String toString() {
            return "TableEntry{" +
                    "key=" + key +
                    ", value='" + value + '\'' +
                    '}';
        }
    }
}
