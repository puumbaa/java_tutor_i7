package part_2.lesson_2.entities;

import part_2.lesson_2.framework.annotations.DefaultValue;

public class User {

    public Long id;

    @DefaultValue("Kazan")
    private String city;

    private String name;
    private String surname;

    public User(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public User() {
    }


    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", city='" + city + '\'' +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", steps=" + steps +
                '}';
    }

    public int getSteps() {
        return steps;
    }

    private int steps;

    public void go(int step){
        steps+=step;
    }
}
