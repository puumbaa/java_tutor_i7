package part_2.lesson_2;

import part_2.lesson_2.entities.User;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) throws Exception{


        Class<User> userClass = User.class;
        Field[] fields = userClass.getDeclaredFields();
        Method goMethod = userClass.getMethod("go", int.class);
        System.out.println(goMethod);

        Constructor<User> constructor = userClass.getConstructor();
        User user = constructor.newInstance();

        goMethod.invoke(user,10);
        goMethod.invoke(user,5);
        System.out.println(user.getSteps());

/*        for(Field f : fields){
            System.out.println(f.getType() + " " + f.getName());
        }*/
    }
}
