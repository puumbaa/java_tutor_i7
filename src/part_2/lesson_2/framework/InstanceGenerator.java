package part_2.lesson_2.framework;


import part_2.lesson_2.framework.annotations.DefaultValue;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class InstanceGenerator {
    // args = "Иванов","Иван"
    public static <T> T generate(Class<T> clazz, Object... args) throws Exception {
        List<Class<?>> argsType = new ArrayList<>();

        for (Object arg : args) {
            argsType.add(arg.getClass());
        }
        Class<?>[] argsTypeAsArray = new Class[argsType.size()];
        argsType.toArray(argsTypeAsArray);

        Constructor<T> constructor = clazz.getConstructor(argsTypeAsArray);
        T newInstance = constructor.newInstance(args);
        processDefaultValue(newInstance);
        return newInstance;
    }

    private static  <T> void processDefaultValue(T obj) {
        Class<T> clazz = (Class<T>) obj.getClass();
        Field[] declaredFields = clazz.getDeclaredFields();

        for (Field field : declaredFields) {
            DefaultValue annotation = field.getAnnotation(DefaultValue.class);

            if (annotation != null){
                String value = annotation.value();
                try {
                    field.setAccessible(true);
                    field.set(obj, value);
                    field.setAccessible(false);
                } catch (IllegalAccessException e) {
                    throw new IllegalArgumentException(e);
                }
            }
        }
    }
}
